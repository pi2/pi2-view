#include "Named.hpp"
#include <utility>

Named::Named(const Named &other) = default;
Named::Named(Named &&other) noexcept = default;
Named &Named::operator=(const Named &other) = default;
Named &Named::operator=(Named &&other) noexcept = default;
Named::~Named() = default;

std::string Named::getName() const { return name; }

Named::Named(std::string name) : name(std::move(name)) {}
