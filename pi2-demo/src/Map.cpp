#include "Map.hpp"
#include "Junction.hpp"
#include "MapWindowWrapper.hpp"
#include <algorithm>
#include <chrono>
#include <cstddef>
#include <limits>
#include <memory>
#include <ratio>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <utility>
#include <vector>

Map::Map(std::istream &is, std::string_view windowTitle)
    : time(0), mapWindow(windowTitle) {
  // Make sure to recognize `.` as decimal mark
  is.imbue(std::locale::classic());

  std::string type;
  while (is >> type) {
    if (type == "KREUZUNG") {
      extractJunctionAndDraw(is, mapWindow);
    } else if (type == "STRASSE") {
      extractRoadAndDraw(is, mapWindow);
    } else if (type == "PKW") {
      extractCar(is);
    } else if (type == "FAHRRAD") {
      extractBicycle(is);
    }
  }
}

void Map::simulate(const std::chrono::seconds duration,
                   const double speedFactor, const double frequency) {
  const auto count =
      static_cast<size_t>(duration.count() * frequency / speedFactor);
  for (size_t i = 0; i < count; i++) {
    time += static_cast<double>(duration.count()) / 60 / 60 / count;
    for (auto &&junction : junctions) {
      junction->processVehicles(time);
      junction->drawVehicles(mapWindow);
    }
    mapWindow.setTimeLabel(std::chrono::minutes(static_cast<int>(time * 60)));
    std::this_thread::sleep_for(std::chrono::nanoseconds(
        static_cast<size_t>(std::nano::den / frequency)));
  }
}

std::weak_ptr<Junction> Map::getJunction(std::string_view name) {
  return *std::find_if(
      junctions.begin(), junctions.end(),
      [&name](const auto &junction) { return junction->getName() == name; });
}

void Map::extractJunctionAndDraw(std::istream &is,
                                 MapWindowWrapper &mapWindow) {
  std::string name;
  double fuelAmount;
  is >> name >> fuelAmount;
  auto junction = std::make_shared<Junction>(name, fuelAmount);
  junctions.push_back(std::move(junction));
  int x;
  int y;
  is >> x >> y;
  mapWindow.addJunction(x, y);
}

void Map::extractRoadAndDraw(std::istream &is, MapWindowWrapper &mapWindow) {
  std::string nameJunctionA;
  std::string nameJunctionB;
  std::string nameLaneAToB;
  std::string nameLaneBToA;
  double lengthRoad;
  int speedLimitLevel;
  bool noPassing;
  is >> nameJunctionA >> nameJunctionB >> nameLaneAToB >> nameLaneBToA >>
      lengthRoad >> speedLimitLevel >> noPassing;

  double speedLimit;
  switch (speedLimitLevel) {
  case 1:
    speedLimit = 50;
    break;
  case 2:
    speedLimit = 100;
    break;
  case 3:
    speedLimit = std::numeric_limits<double>::infinity();
    break;
  default:
    throw std::invalid_argument('\'' + std::to_string(speedLimitLevel) +
                                "' does not encode a speed limit.");
  }

  auto junctionA = getJunction(nameJunctionA);
  auto junctionB = getJunction(nameJunctionB);
  Junction::connect(junctionA, junctionB, nameLaneAToB, nameLaneBToA,
                    lengthRoad, speedLimit, noPassing);
  int coordinatePairCount;
  is >> coordinatePairCount;
  std::vector<double> coordinates(coordinatePairCount * 2);
  std::generate(coordinates.begin(), coordinates.end(), [&is]() {
    double x;
    is >> x;
    return x;
  });
  mapWindow.addRoad(nameLaneAToB, nameLaneBToA, coordinates);
}

void Map::extractBicycle(std::istream &is) {
  std::string name;
  double maximumVelocity;
  is >> name >> maximumVelocity;
  std::string nameStartJunction;
  double timeOfStart;
  is >> nameStartJunction >> timeOfStart;
  auto &junction = *getJunction(nameStartJunction).lock();
  junction.createBicycle(name, maximumVelocity, timeOfStart, time);
}

void Map::extractCar(std::istream &is) {
  std::string name;
  double maximumVelocity;
  double fuelConsumption;
  double fuelCapacity;
  is >> name >> maximumVelocity >> fuelConsumption >> fuelCapacity;
  std::string nameStartJunction;
  double timeOfStart;
  is >> nameStartJunction >> timeOfStart;
  auto &junction = *getJunction(nameStartJunction).lock();
  junction.createCar(name, maximumVelocity, fuelConsumption, fuelCapacity,
                     timeOfStart, time);
}
