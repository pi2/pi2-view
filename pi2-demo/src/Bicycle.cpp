#include "Bicycle.hpp"
#include "MapWindowWrapper.hpp"
#include <algorithm>
#include <cmath>
#include <utility>

Bicycle::Bicycle(std::string name, const double topSpeed,
                 const double timeOfStart, const double time)
    : Vehicle(std::move(name), topSpeed, timeOfStart, time) {}

void Bicycle::draw(MapWindowWrapper &mapWindow, std::string_view laneName,
                   const double laneLength, const double speedLimit) const {
  const auto positionOnLane = getDistanceOnLane() / laneLength;
  mapWindow.addOrReplaceBicycle(getName(), laneName, positionOnLane,
                                getSpeed(speedLimit));
}

double Bicycle::getSpeed(const double speedLimit) const {
  auto const slowingSpeed =
      getTopSpeed() * std::pow(0.9, std::floor(getGesamtStrecke() / 20));
  return isParking() ? 0 : std::min(std::max(slowingSpeed, 12.0), speedLimit);
}
