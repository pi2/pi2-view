#include "Lane.hpp"
#include "Bicycle.hpp"
#include "Car.hpp"
#include "Junction.hpp"
#include "Vehicle.hpp"
#include <algorithm>
#include <memory>
#include <utility>
class MapWindowWrapper;

// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
Lane::Lane(Lane &&other) noexcept = default;

Lane::Lane(std::string name, std::string oppositeLaneName, const double length,
           const double speedLimit, const bool noPassing,
           std::weak_ptr<Junction> destinationJunction)
    : Named(std::move(name)), length(length),
      destinationJunction(std::move(destinationJunction)),
      oppositeLaneName(std::move(oppositeLaneName)), speedLimit(speedLimit),
      noPassing(noPassing) {}

void Lane::drawVehicles(MapWindowWrapper &mapWindow) const {
  for (auto &&fahrzeug : vehicles) {
    fahrzeug->draw(mapWindow, getName(), length, speedLimit);
  }
}

void Lane::processVehicles(const double time) {
  const auto furthestFirstComparator = [](const auto &vehicleA,
                                          const auto &vehicleB) {
    return vehicleA->getDistanceOnLane() > vehicleB->getDistanceOnLane();
  };
  std::sort(vehicles.begin(), vehicles.end(), furthestFirstComparator);
  double maximumPosition = length;
  for (auto &&fahrzeug : vehicles) {
    fahrzeug->process(time, maximumPosition, speedLimit);
    if (noPassing && fahrzeug->getSpeed(speedLimit) != 0) {
      maximumPosition = fahrzeug->getDistanceOnLane();
    }
  }
  std::sort(vehicles.begin(), vehicles.end(), furthestFirstComparator);
  while (!vehicles.empty() && length <= vehicles.at(0)->getDistanceOnLane()) {
    destinationJunction.lock()->acceptVehicle(std::move(vehicles.front()),
                                              oppositeLaneName);
    vehicles.erase(vehicles.begin());
  }
}

void Lane::acceptVehicle(std::unique_ptr<Vehicle> vehicle) {
  vehicles.push_back(std::move(vehicle));
}

void Lane::createCar(std::string name, const double maximumVelocity,
                     const double fuelConsumption, const double fuelCapacity,
                     const double timeOfStart, const double time) {
  vehicles.push_back(std::make_unique<Car>(std::move(name), maximumVelocity,
                                           timeOfStart, fuelConsumption,
                                           fuelCapacity, time));
}

void Lane::createBicycle(std::string name, const double maximumVelocity,
                         const double timeOfStart, const double time) {
  vehicles.push_back(std::make_unique<Bicycle>(std::move(name), maximumVelocity,
                                               timeOfStart, time));
}
