#include "Map.hpp"
#include <boost/program_options.hpp>
#include <chrono>
#include <fstream>
#include <gsl/span>
#include <iostream>
#include <ratio>
#include <string>
#include <type_traits>
namespace po = boost::program_options;

int main(int argc, char *argv[]) {
  auto args = gsl::make_span(argv, argc);

  try {
    std::string durationString;
    double timescale;
    double refreshRate;
    std::string nativePathname;

    po::options_description visibleOptions("Allowed options");
    visibleOptions.add_options()(
        "duration,t",
        po::value<std::string>(&durationString)->default_value("20:00:00"),
        "simulate this amount of time")(
        "timescale,s", po::value<double>(&timescale)->default_value(30 * 60),
        "scale the simulation time with this factor")(
        "rate,r", po::value<double>(&refreshRate)->default_value(60),
        "refresh the simulation with this frequency")(
        "help", "display this help and exit");

    po::options_description hiddenOptions("Hidden options");
    hiddenOptions.add_options()(
        "input-file", po::value<std::string>(&nativePathname), "input file");

    po::options_description allOptions;
    allOptions.add(visibleOptions).add(hiddenOptions);

    po::positional_options_description positionalOptions;
    positionalOptions.add("input-file", -1);

    po::variables_map variablesMap;
    po::store(po::command_line_parser(argc, argv)
                  .options(allOptions)
                  .positional(positionalOptions)
                  .run(),
              variablesMap);
    po::notify(variablesMap);

    if (variablesMap.count("help") > 0) {
      std::cout
          << "Usage: " << args.at(0)
          << " [OPTION]... [FILE]\n"
             "Parses and simulates FILE according to the PI-2 of the RWTH.\n\n"
          << visibleOptions;
      return 0;
    }

    if (variablesMap.count("input-file") > 0) {
      std::ifstream istrm(nativePathname);
      if (istrm.is_open()) {
        Map map(istrm, "pi2-demo - " + nativePathname);

        std::istringstream buffer(durationString);
        int hours;
        int minutes = 0;
        int seconds = 0;
        buffer >> hours;
        buffer.ignore();
        buffer >> minutes;
        buffer.ignore();
        buffer >> seconds;

        std::chrono::seconds duration = std::chrono::hours(hours) +
                                        std::chrono::minutes(minutes) +
                                        std::chrono::seconds(seconds);

        map.simulate(duration, timescale, refreshRate);
        std::cout << "Simulation done!\n";
      } else {
        std::cout << "Failed to open " << nativePathname << '\n';
        return 1;
      }
    } else {
      std::cout << "Input file was not set.\n";
    }
  } catch (std::exception &e) {
    std::cout << e.what() << '\n';
    return 1;
  }
}
