#include "Junction.hpp"
#include "Car.hpp"
#include "Vehicle.hpp"
#include <memory>
#include <random>
#include <typeinfo>
#include <utility>
class MapWindowWrapper;

void Junction::connect(const std::weak_ptr<Junction> &junctionA,
                       const std::weak_ptr<Junction> &junctionB,
                       std::string laneAToBName, std::string laneBToAName,
                       const double length, const double speedLimit,
                       const bool noPassing) {
  Lane laneAToB(laneAToBName, laneBToAName, length, speedLimit, noPassing,
                junctionB);
  Lane laneBToA(std::move(laneBToAName), std::move(laneAToBName), length,
                speedLimit, noPassing, junctionA);
  junctionA.lock()->outboundLanes.push_back(std::move(laneAToB));
  junctionB.lock()->outboundLanes.push_back(std::move(laneBToA));
}

// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
Junction::Junction(Junction &&other) noexcept = default;
Junction &Junction::operator=(Junction &&other) noexcept = default;

Junction::Junction(std::string name, const double fuelAmount)
    : Named(std::move(name)), fuelAmount(fuelAmount) {}

void Junction::processVehicles(const double time) {
  for (auto &&lane : outboundLanes) {
    lane.processVehicles(time);
  }
}

void Junction::drawVehicles(MapWindowWrapper &mapWindow) const {
  for (auto &&weg : outboundLanes) {
    weg.drawVehicles(mapWindow);
  }
}

void Junction::createCar(std::string name, const double topSpeed,
                         const double fuelConsumption,
                         const double fuelCapacity, const double timeOfStart,
                         const double time) {
  randomOutboundLane().createCar(std::move(name), topSpeed, fuelConsumption,
                                 fuelCapacity, timeOfStart, time);
}

void Junction::createBicycle(std::string name, const double topSpeed,
                             const double timeOfStart, const double time) {
  randomOutboundLane().createBicycle(std::move(name), topSpeed, timeOfStart,
                                     time);
}

void Junction::acceptVehicle(std::unique_ptr<Vehicle> vehicle,
                             std::string_view prohibitedLaneName) {
  fillUp(*vehicle);
  vehicle->resetDistanceOnLane();
  randomOutboundLaneExcept(prohibitedLaneName)
      .acceptVehicle(std::move(vehicle));
}

std::mt19937 Junction::gen([]() {
  std::random_device rd;
  return rd();
}());

void Junction::fillUp(Vehicle &fahrzeug) {
  try {
    auto &car = dynamic_cast<Car &>(fahrzeug);
    fuelAmount -= car.refuel(fuelAmount);
  } catch (const std::bad_cast &e) {
  }
}

Lane &Junction::randomOutboundLane() {
  std::uniform_int_distribution<> dis(0, outboundLanes.size() - 1);
  return outboundLanes.at(dis(gen));
}

Lane &Junction::randomOutboundLaneExcept(std::string_view laneName) {
  auto &randomLane = randomOutboundLane();
  return randomLane.getName() != laneName || outboundLanes.size() <= 1
             ? randomLane
             : randomOutboundLaneExcept(laneName);
}
