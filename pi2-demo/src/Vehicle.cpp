#include "Vehicle.hpp"
#include <algorithm>
#include <utility>

void Vehicle::process(const double time, const double maximumDistanceOnLane,
                      const double speedLimit) {
  if (parking && time >= timeOfStart) {
    parking = false;
  }

  if (!parking) {
    const auto possibleDistance = maximumDistanceOnLane - getDistanceOnLane();
    const auto potentialDistance =
        (time - timeOfLastProcessing) * getSpeed(speedLimit);
    const auto deltaDistance = std::min(possibleDistance, potentialDistance);
    distanceOnLane += deltaDistance;
    odometer += deltaDistance;
  }

  timeOfLastProcessing = time;
}

double Vehicle::getDistanceOnLane() const { return distanceOnLane; }

void Vehicle::resetDistanceOnLane() { distanceOnLane = 0; }

Vehicle::Vehicle(std::string name, const double topSpeed,
                 const double timeOfStart, const double time)
    : Named(std::move(name)), topSpeed(topSpeed), odometer(0),
      distanceOnLane(0), parking(true), timeOfStart(timeOfStart),
      timeOfLastProcessing(time) {}

double Vehicle::getTopSpeed() const { return topSpeed; }

double Vehicle::getGesamtStrecke() const { return odometer; }

bool Vehicle::isParking() const { return parking; }
