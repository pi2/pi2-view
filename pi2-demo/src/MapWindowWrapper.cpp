#include "MapWindowWrapper.hpp"
#include <pi2-view.h>
#include <string_view>
#include <type_traits>
#include <vector>

/// Wraps MapWindow of pi2-view.h
/// Uses custom deleter of std::unique_ptr to properly destroy mapWindow
MapWindowWrapper::MapWindowWrapper(std::string_view windowTitle)
    : mapWindow(mapWindowCreate(windowTitle.data()),
                [](MapWindow *mapWindow) { mapWindowDestroy(mapWindow); }) {}

void MapWindowWrapper::addJunction(const double x, const double y) {
  mapWindowAddJunction(mapWindow.get(), x, y);
}

void MapWindowWrapper::addRoad(std::string_view laneThereName,
                               std::string_view laneBackName,
                               const std::vector<double> &coordinates) {
  mapWindowAddRoad(mapWindow.get(), laneThereName.data(), laneBackName.data(),
                   coordinates.data(), coordinates.size());
}

void MapWindowWrapper::addOrReplaceBicycle(std::string_view vehicleName,
                                           std::string_view laneName,
                                           const double positionOnLane,
                                           const double speed) {
  mapWindowAddOrReplaceBicycle(mapWindow.get(), vehicleName.data(),
                               laneName.data(), positionOnLane, speed);
}

void MapWindowWrapper::addOrReplaceCar(std::string_view vehicleName,
                                       std::string_view laneName,
                                       const double positionOnLane,
                                       const double speed,
                                       const double remainingFuel) {
  mapWindowAddOrReplaceCar(mapWindow.get(), vehicleName.data(), laneName.data(),
                           positionOnLane, speed, remainingFuel);
}

void MapWindowWrapper::setTimeLabel(const std::chrono::minutes time) {
  mapWindowSetTimeLabel(mapWindow.get(), time.count());
}
