#include "Car.hpp"
#include "MapWindowWrapper.hpp"
#include <algorithm>
#include <utility>

Car::Car(std::string name, const double topSpeed, const double timeOfStart,
         const double fuelConsumption, const double fuelCapacity,
         const double time)
    : Vehicle(std::move(name), topSpeed, timeOfStart, time),
      fuelConsumption(fuelConsumption), fuelCapacity(fuelCapacity),
      fuel(fuelCapacity / 2) {}

void Car::draw(MapWindowWrapper &mapWindow, std::string_view laneName,
               const double laneLength, const double speedLimit) const {
  const auto positionOnLane = getDistanceOnLane() / laneLength;
  mapWindow.addOrReplaceCar(getName(), laneName, positionOnLane,
                            getSpeed(speedLimit), fuel);
}

double Car::getSpeed(const double speedLimit) const {
  const auto potentialSpeed = std::min(speedLimit, getTopSpeed());
  return isParking() || fuel <= 0 ? 0 : potentialSpeed;
}

void Car::process(const double time, const double maximumDistanceOnLane,
                  const double speedLimit) {
  const auto oldDistance = getGesamtStrecke();
  Vehicle::process(time, maximumDistanceOnLane, speedLimit);
  fuel -= fuelConsumption * (getGesamtStrecke() - oldDistance) / 100;
}

double Car::refuel(const double availableFuel) {
  const auto refueledAmount = std::min(availableFuel, fuelCapacity - fuel);
  fuel += refueledAmount;
  return refueledAmount;
}
