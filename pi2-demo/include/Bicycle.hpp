#pragma once

#include "Vehicle.hpp"
#include <string>
#include <string_view>
class MapWindowWrapper;

class Bicycle : public Vehicle {
public:
  Bicycle(std::string name, double topSpeed, double timeOfStart, double time);

  void draw(MapWindowWrapper &mapWindow, std::string_view laneName,
            double laneLength, double speedLimit) const override;

  double getSpeed(double speedLimit) const override;
};
