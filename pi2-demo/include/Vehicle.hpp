#pragma once

#include "Named.hpp"
#include <string>
#include <string_view>
class MapWindowWrapper;

class Vehicle : public Named {
public:
  // Draws the vehicle on the provided \p mapWindow on the specified \p laneName
  // which has a \p laneLength and a \p speedLimit
  virtual void draw(MapWindowWrapper &mapWindow, std::string_view laneName,
                    double laneLength, double speedLimit) const = 0;

  /// \returns The current speed in km/h
  virtual double getSpeed(double speedLimit) const = 0;

  /// Updates the total traveled distance, the total traveled time and
  /// the time of this tick
  virtual void process(double time, double maximumDistanceOnLane,
                       double speedLimit);

  double getDistanceOnLane() const;

  void resetDistanceOnLane();

protected:
  Vehicle(std::string name, double topSpeed, double timeOfStart, double time);

  double getTopSpeed() const;

  double getGesamtStrecke() const;

  bool isParking() const;

private:
  /// Maximum speed in km/h
  double topSpeed;

  /// Total travel distance in km
  double odometer;

  /// Current distance on lane in km
  double distanceOnLane;

  bool parking;

  double timeOfStart;

  double timeOfLastProcessing;
};
