#pragma once

#include "Vehicle.hpp"
#include <string>
#include <string_view>
class MapWindowWrapper;

class Car : public Vehicle {
public:
  /// The fuel tank is half full at construction
  Car(std::string name, double topSpeed, double timeOfStart,
      double fuelConsumption, double fuelCapacity, double time);

  void draw(MapWindowWrapper &mapWindow, std::string_view laneName,
            double laneLength, double speedLimit) const override;

  double getSpeed(double speedLimit) const override;

  void process(double time, double maximumDistanceOnLane,
               double speedLimit) override;

  /// Refuels the vehicle provided the \p availableFuel in l
  /// \returns The amount of fuel in l that has been taken
  double refuel(double availableFuel);

private:
  /// Fuel consumption in l/100km
  double fuelConsumption;

  /// Fuel capacity in l
  double fuelCapacity;

  /// Remaining fuel in l
  double fuel;
};
