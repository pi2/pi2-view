#pragma once

#include "Named.hpp"
#include "Vehicle.hpp"
#include <memory>
#include <string>
#include <vector>
class MapWindowWrapper;
class Junction;

class Lane : public Named {
public:
  Lane(Lane &&other) noexcept;

  Lane(std::string name, std::string oppositeLaneName, double length,
       double speedLimit, bool noPassing,
       std::weak_ptr<Junction> destinationJunction);

  void drawVehicles(MapWindowWrapper &mapWindow) const;

  void processVehicles(double time);

  void acceptVehicle(std::unique_ptr<Vehicle> vehicle);

  void createCar(std::string name, double maximumVelocity,
                 double fuelConsumption, double fuelCapacity,
                 double timeOfStart, double time);

  void createBicycle(std::string name, double maximumVelocity,
                     double timeOfStart, double time);

private:
  /// Length property in km
  double length;

  std::weak_ptr<Junction> destinationJunction;

  std::string oppositeLaneName;

  double speedLimit;

  bool noPassing;

  std::vector<std::unique_ptr<Vehicle>> vehicles;
};
