#pragma once

#include "MapWindowWrapper.hpp"
#include <chrono>
#include <iosfwd>
#include <memory>
#include <string_view>
#include <vector>
class Junction;

class Map {
public:
  Map(std::istream &is, std::string_view windowTitle);

  void simulate(std::chrono::seconds duration, double speedFactor,
                double frequency);

private:
  double time;
  std::vector<std::shared_ptr<Junction>> junctions;
  MapWindowWrapper mapWindow;

  std::weak_ptr<Junction> getJunction(std::string_view name);

  /// Required format:
  /// `name` `fuelAmount` `x` `y`
  void extractJunctionAndDraw(std::istream &is, MapWindowWrapper &mapWindow);

  /// Required format:
  /// `nameJunctionA` `nameJunctionB` `nameLaneAToB` `nameLaneBToA`
  /// `length in km` `speedLimit in {1,2,3}` `noPassing` `coordinateCount`
  /// `coordinates...`
  void extractRoadAndDraw(std::istream &is, MapWindowWrapper &mapWindow);

  /// Required format:
  /// `name` `topSpeed` `nameStartJunction` `timeOfStart`
  void extractBicycle(std::istream &is);

  /// Required format:
  /// `name` `topSpeed` `fuelConsumption` `fuelCapacity` `nameStartJunction`
  /// `timeOfStart`
  void extractCar(std::istream &is);
};
