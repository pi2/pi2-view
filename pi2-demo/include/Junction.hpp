#pragma once

#include "Lane.hpp"
#include "Named.hpp"
#include <memory>
#include <random>
#include <string>
#include <string_view>
#include <vector>
class Vehicle;
class MapWindowWrapper;

class Junction : public Named {
public:
  /// Creates two lanes between \p junctionA and \p junctionB
  static void connect(const std::weak_ptr<Junction> &junctionA,
                      const std::weak_ptr<Junction> &junctionB,
                      std::string laneAToBName, std::string laneBToAName,
                      double length, double speedLimit, bool noPassing);

  Junction(Junction &&other) noexcept;
  Junction &operator=(Junction &&other) noexcept;

  Junction(std::string name, double fuelAmount);

  void processVehicles(double time);

  void drawVehicles(MapWindowWrapper &mapWindow) const;

  void createCar(std::string name, double topSpeed, double fuelConsumption,
                 double fuelCapacity, double timeOfStart, double time);

  void createBicycle(std::string name, double topSpeed, double timeOfStart,
                     double time);

  void acceptVehicle(std::unique_ptr<Vehicle> vehicle,
                     std::string_view prohibitedLaneName);

private:
  /// Mersenne Twister random number generator
  /// for selection in randomOutboundLaneExcept
  static std::mt19937 gen;

  std::vector<Lane> outboundLanes;

  /// The amount of fuel in l in the filling station
  double fuelAmount;

  /// Refuels the given vehicle using the filling station
  void fillUp(Vehicle &fahrzeug);

  Lane &randomOutboundLane();

  Lane &randomOutboundLaneExcept(std::string_view laneName);
};
