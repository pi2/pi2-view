#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <string_view>
#include <vector>
struct MapWindow;

/// A window showing a map, a list of vehicles and additional information
class MapWindowWrapper {
public:
  /// Opens a MapWindow with \p windowTitle.
  MapWindowWrapper(std::string_view windowTitle);

  /// Adds a junction
  ///
  /// The coordinates \p x and \p y mark the middle of the junction and
  /// represent amounts of pixels.
  void addJunction(double x, double y);

  /// Adds a road
  ///
  /// The coordinates have to be alternating values of x and y in pixels.
  void addRoad(std::string_view laneThereName, std::string_view laneBackName,
               const std::vector<double> &coordinates);

  /// Adds or replaces a bicycle
  ///
  /// \param positionOnLane The relative position of the vehicle on its lane
  /// has to be between 0 and 1.
  void addOrReplaceBicycle(std::string_view vehicleName,
                           std::string_view laneName, double positionOnLane,
                           double speed);

  /// Adds or replaces a car
  ///
  /// \param positionOnLane The relative position of the vehicle on its lane
  /// has to be between 0 and 1.
  void addOrReplaceCar(std::string_view vehicleName, std::string_view laneName,
                       double positionOnLane, double speed,
                       double remainingFuel);

  /// Sets the time label to \p time
  void setTimeLabel(std::chrono::minutes time);

private:
  std::unique_ptr<MapWindow, std::function<void(MapWindow *)>> mapWindow;
};
