#pragma once

#include <string>

class Named {
public:
  Named(const Named &other);
  Named(Named &&other) noexcept;
  Named &operator=(const Named &other);
  Named &operator=(Named &&other) noexcept;
  virtual ~Named() = 0;

  std::string getName() const;

protected:
  explicit Named(std::string name);

private:
  std::string name;
};
