#include "pi2-view.h"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmismatched-tags"
#include "MapWindow.hpp"
#pragma clang diagnostic pop
#include "MapApplication.hpp"
#include <algorithm>
#include <chrono>
#include <gsl/pointers>
#include <gsl/span>
#include <gsl/string_span>
#include <string>
#include <valarray>
#include <vector>

gsl::owner<MapWindow *> mapWindowCreate(gsl::czstring<> windowTitle) {
  auto mapWindow = new MapWindow(windowTitle);
  MapApplication::addWindow(*mapWindow);
  return mapWindow;
}

void mapWindowDestroy(gsl::owner<MapWindow *> mapWindow) {
  MapApplication::removeWindow(*mapWindow);
  delete mapWindow;
}

void mapWindowAddJunction(MapWindow *mapWindow, const double x,
                          const double y) {
  mapWindow->addJunction(x, y);
}

void mapWindowAddRoad(MapWindow *mapWindow, gsl::czstring<> laneThereName,
                      gsl::czstring<> laneBackName, const double coordinates[],
                      const size_t coordinateCount) {
  const gsl::span<const double> coordinatesSpan(coordinates, coordinateCount);
  std::vector<std::valarray<double>> coordinateVector(coordinateCount / 2);
  std::generate(coordinateVector.begin(), coordinateVector.end(),
                [&coordinatesSpan, i = 0]() mutable {
                  return std::valarray<double>(
                      {coordinatesSpan.at(i++), coordinatesSpan(i++)});
                });
  mapWindow->addRoad(laneThereName, laneBackName, coordinateVector);
}

void mapWindowAddOrReplaceBicycle(MapWindow *mapWindow,
                                  gsl::czstring<> vehicleName,
                                  gsl::czstring<> roadName,
                                  const double positionOnLane,
                                  const double speed) {
  mapWindow->addOrReplaceBicycle(vehicleName, roadName, positionOnLane, speed);
}

void mapWindowAddOrReplaceCar(MapWindow *mapWindow, gsl::czstring<> vehicleName,
                              gsl::czstring<> roadName,
                              const double positionOnLane, const double speed,
                              const double remainingFuel) {
  mapWindow->addOrReplaceCar(vehicleName, roadName, positionOnLane, speed,
                             remainingFuel);
}

void mapWindowSetTimeLabel(MapWindow *mapWindow, int64_t minutes) {
  mapWindow->setTimeLabel(std::chrono::minutes(minutes));
}
