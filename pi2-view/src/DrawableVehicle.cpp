#include "DrawableVehicle.hpp"
#include "DrawableRoad.hpp"
#include <cmath>
#include <utility>

const double DrawableVehicle::radius = 5;

DrawableVehicle::DrawableVehicle(std::string carName, std::string laneName,
                                 const DrawableRoad &road,
                                 const double positionOnLane,
                                 const double speed, const double remainingFuel,
                                 const double red, const double green,
                                 const double blue)
    : xc(0), yc(0), carName(std::move(carName)), laneName(std::move(laneName)),
      positionOnLane(positionOnLane), speed(speed),
      remainingFuel(remainingFuel), red(red), green(green), blue(blue) {
  road.getCoordinatesOf(this->laneName, positionOnLane, xc, yc);
}

void DrawableVehicle::draw(const Cairo::RefPtr<Cairo::Context> &cr) const {
  cr->save();

  cr->set_source_rgb(red, green, blue);
  cr->arc(xc, yc, radius, 0, 2 * std::acos(-1));
  cr->fill();

  cr->restore();
}

void DrawableVehicle::getRegion(int &x, int &y, int &width, int &height) const {
  x = static_cast<int>(std::floor(xc - radius)) - 1;
  y = static_cast<int>(std::floor(yc - radius)) - 1;
  width = height = static_cast<int>(std::ceil(2 * radius)) + 2;
}

std::string DrawableVehicle::getLaneName() const { return laneName; }

double DrawableVehicle::getPositionOnLane() const { return positionOnLane; }

double DrawableVehicle::getSpeed() const { return speed; }

double DrawableVehicle::getRemainingFuel() const { return remainingFuel; }
