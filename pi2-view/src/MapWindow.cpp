#include "MapWindow.hpp"
#include <algorithm>
#include <cmath>
#include <glibmm/main.h>
#include <glibmm/priorities.h>
#include <glibmm/signalproxy.h>
#include <glibmm/ustring.h>
#include <gtkmm/box.h>
#include <gtkmm/builder.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/listbox.h>
#include <gtkmm/listboxrow.h>
#include <gtkmm/object.h>
#include <gtkmm/requisition.h>
#include <gtkmm/viewport.h>
#include <gtkmm/widget.h>
#include <iomanip>
#include <iostream>
#include <sigc++/functors/mem_fun.h>
#include <utility>

const std::vector<std::string> MapWindow::zoomLevels = {
    "50%",  "70%",  "85%",  "100%", "125%",
    "150%", "175%", "200%", "300%", "400%"};

MapWindow::MapWindow(std::string windowTitle)
    : mapArea(junctions, roads, nameVehicleMap), listBox(nullptr),
      timeLabel(nullptr), laneLabel(nullptr),
      lanePositionPercentageLabel(nullptr), speedLabel(nullptr),
      remainingFuelLabel(nullptr), zoomComboBoxText(nullptr) {
  set_title(std::move(windowTitle));

  auto builder = Gtk::Builder::create();
  builder->set_translation_domain("pi2-view");
  builder->add_from_resource("/pi2-view/pi2-view.glade", "box");
  auto box = builder->get_widget<Gtk::Box>("box");

  auto viewport = builder->get_widget<Gtk::Viewport>("viewport");

  listBox = builder->get_widget<Gtk::ListBox>("listBox");

  timeLabel = builder->get_widget<Gtk::Label>("timeLabel");
  laneLabel = builder->get_widget<Gtk::Label>("laneLabel");
  lanePositionPercentageLabel =
      builder->get_widget<Gtk::Label>("lanePositionPercentageLabel");
  speedLabel = builder->get_widget<Gtk::Label>("speedLabel");
  remainingFuelLabel = builder->get_widget<Gtk::Label>("remainingFuelLabel");

  zoomComboBoxText = builder->get_widget<Gtk::ComboBoxText>("zoomComboBoxText");
  for (auto &&zoomLevel : zoomLevels) {
    zoomComboBoxText->append(zoomLevel);
  }
  zoomComboBoxText->set_active_text("100%");
  zoomComboBoxText->signal_changed().connect(
      sigc::mem_fun(*this, &MapWindow::onZoomSelectionChanged));

  listBox->signal_row_selected().connect(
      sigc::mem_fun(*this, &MapWindow::onVehicleRowSelected));

  set_child(*box);
  viewport->set_child(mapArea);
  mapArea.show();
}

void MapWindow::onZoomSelectionChanged() {
  std::string selection = zoomComboBoxText->get_active_text();
  auto levelResult = std::find(zoomLevels.begin(), zoomLevels.end(), selection);
  if (levelResult != zoomLevels.end()) {
    zoomComboBoxText->set_active_text(selection);
    double zoom;
    std::istringstream zoomBuffer(selection);
    zoomBuffer >> zoom;
    mapArea.setScale(zoom / 100);
    Glib::signal_idle().connect_once([this]() { mapArea.adaptSizeRequest(); },
                                     Glib::PRIORITY_HIGH_IDLE + 10);
  }
}

MapWindow::~MapWindow() = default;

void MapWindow::onVehicleRowSelected(Gtk::ListBoxRow *row) {
  auto &label = dynamic_cast<Gtk::Label &>(*row->get_child());
  auto &vehicle = nameVehicleMap.at(label.get_text());
  refreshVehicleLabels(vehicle);
}

void MapWindow::refreshVehicleLabels(const DrawableVehicle &vehicle) {
  laneLabel->set_text(vehicle.getLaneName());

  std::ostringstream lanePisitionPercentageBuffer;
  lanePisitionPercentageBuffer
      << std::setw(3) << std::round(vehicle.getPositionOnLane() * 100) << " %";
  lanePositionPercentageLabel->set_text(lanePisitionPercentageBuffer.str());

  std::ostringstream speedBuffer;
  speedBuffer << std::fixed << std::setprecision(1) << vehicle.getSpeed()
              << " km/h";
  speedLabel->set_text(speedBuffer.str());

  std::ostringstream remainingFuelBuffer;
  remainingFuelBuffer << std::fixed << std::setprecision(1)
                      << vehicle.getRemainingFuel() << " l";
  remainingFuelLabel->set_text(remainingFuelBuffer.str());
}

void MapWindow::setTimeLabel(const std::chrono::minutes time) {
  std::ostringstream timeBuffer;
  timeBuffer << std::setfill('0') << std::setw(2)
             << std::chrono::duration_cast<std::chrono::hours>(time).count()
             << ':' << std::setw(2) << time.count() % 60;
  Glib::signal_idle().connect_once(
      [this, timeString = timeBuffer.str()]() mutable {
        timeLabel->set_text(std::move(timeString));
      },
      Glib::PRIORITY_HIGH_IDLE + 20);
}

void MapWindow::addJunction(const double x, const double y) {
  auto &junction = junctions.emplace_back(x, y);
  Glib::signal_idle().connect_once(
      [this]() {
        mapArea.adaptSizeRequest();
        set_default_size(-1, -1);
      },
      Glib::PRIORITY_HIGH_IDLE + 10);
  int xRegion, yRegion, widthRegion, heightRegion;
  junction.getRegion(xRegion, yRegion, widthRegion, heightRegion);
  mapArea.queue_draw();
}

void MapWindow::addRoad(std::string laneThereName, std::string laneBackName,
                        std::vector<std::valarray<double>> coordinates) {
  auto &road = roads.emplace_back(laneThereName, laneBackName, coordinates);
  Glib::signal_idle().connect_once(
      [this]() {
        mapArea.adaptSizeRequest();
        set_default_size(-1, -1);
      },
      Glib::PRIORITY_HIGH_IDLE + 10);
  int xRegion, yRegion, widthRegion, heightRegion;
  road.getRegion(xRegion, yRegion, widthRegion, heightRegion);
  mapArea.queue_draw();
}

void MapWindow::addOrReplaceBicycle(const std::string &vehicleName,
                                    const std::string &laneName,
                                    const double positionOnLane,
                                    const double speed) {
  addOrReplaceVehicle(vehicleName, laneName, positionOnLane, speed, 0, 0, 1, 0);
}

void MapWindow::addOrReplaceCar(const std::string &vehicleName,
                                const std::string &laneName,
                                const double positionOnLane, const double speed,
                                const double remainingFuel) {
  addOrReplaceVehicle(vehicleName, laneName, positionOnLane, speed,
                      remainingFuel, 1, 0, 0);
}

void MapWindow::addOrReplaceVehicle(const std::string &vehicleName,
                                    const std::string &laneName,
                                    const double positionOnLane,
                                    const double speed,
                                    const double remainingFuel, double red,
                                    double green, double blue) {
  auto &road = *std::find_if(roads.begin(), roads.end(),
                             [&laneName](const DrawableRoad &road) {
                               return road.contains(laneName);
                             });
  auto selectedRow = listBox->get_selected_row();
  if (selectedRow != nullptr) {
    auto selectedVehicleName =
        dynamic_cast<Gtk::Label &>(*selectedRow->get_child()).get_text();
    if (selectedVehicleName.raw() == vehicleName) {
      red = green = blue = 1;
    }
  }

  auto search = nameVehicleMap.find(vehicleName);
  int oldX = 0;
  int oldY = 0;
  int oldWidth = 0;
  int oldHeight = 0;
  if (search != nameVehicleMap.end()) {
    auto &foundVehicle = (*search).second;
    foundVehicle.getRegion(oldX, oldY, oldWidth, oldHeight);
  }

  auto insertion = nameVehicleMap.insert_or_assign(
      vehicleName, DrawableVehicle(vehicleName, laneName, road, positionOnLane,
                                   speed, remainingFuel, red, green, blue));

  auto &insertedVehicle = insertion.first->second;
  int newX, newY, newWidth, newHeight;
  insertedVehicle.getRegion(newX, newY, newWidth, newHeight);

  mapArea.queue_draw();

  if (selectedRow != nullptr) {
    auto selectedVehicleName =
        dynamic_cast<Gtk::Label &>(*selectedRow->get_child()).get_text();
    if (selectedVehicleName.raw() == vehicleName) {
      Glib::signal_idle().connect_once(
          [this, &insertedVehicle]() { refreshVehicleLabels(insertedVehicle); },
          Glib::PRIORITY_HIGH_IDLE + 20);
    }
  }

  if (insertion.second) {
    auto *label = Gtk::make_managed<Gtk::Label>();
    label->set_text(vehicleName);
    auto *listBoxRow = Gtk::make_managed<Gtk::ListBoxRow>();
    listBoxRow->set_child(*label);
    listBox->append(*listBoxRow);
    listBoxRow->show();
    listBox->select_row(*listBoxRow);
  }
}
