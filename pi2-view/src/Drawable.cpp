#include "Drawable.hpp"
#include <stdexcept>

std::function<bool(const Drawable &drawableA, const Drawable &drawableB)>
Drawable::comparator(DrawableTrait drawableTrait) {
  return [drawableTrait](const Drawable &drawableA, const Drawable &drawableB) {
    int xA, yA, widthA, heightA;
    drawableA.getRegion(xA, yA, widthA, heightA);
    int xB, yB, widthB, heightB;
    drawableB.getRegion(xB, yB, widthB, heightB);
    switch (drawableTrait) {
    case DrawableTrait::X:
      return xA < xB;
    case DrawableTrait::Y:
      return yA < yB;
    case DrawableTrait::XPlusWidth:
      return xA + widthA < xB + widthB;
    case DrawableTrait::YPlusHeight:
      return yA + heightA < yB + heightB;
    default:
      throw std::invalid_argument("DrawableTrait unknown");
    }
  };
}

Drawable::Drawable(const Drawable &other) = default;
Drawable::Drawable(Drawable &&other) noexcept = default;
Drawable &Drawable::operator=(const Drawable &other) = default;
Drawable &Drawable::operator=(Drawable &&other) noexcept = default;
Drawable::~Drawable() = default;
Drawable::Drawable() = default;
