#include "DrawableRoad.hpp"
#include <algorithm>
#include <cairomm/enums.h>
#include <cmath>
#include <ext/alloc_traits.h>
#include <limits>
#include <stdexcept>
#include <utility>

const double DrawableRoad::width = 30;

DrawableRoad::DrawableRoad(std::string laneThereName, std::string laneBackName,
                           std::vector<std::valarray<double>> coordinates)
    : laneThereName(std::move(laneThereName)),
      laneBackName(std::move(laneBackName)), pixelLength(0),
      coordinates(coordinates) {
  auto &lastCoordinatePair = coordinates.front();
  for (auto &coordinatePair : coordinates) {
    auto nextStep = coordinatePair - lastCoordinatePair;
    pixelLength += std::hypot(nextStep[0], nextStep[1]);
    lastCoordinatePair = coordinatePair;
  }
}

void DrawableRoad::draw(const Cairo::RefPtr<Cairo::Context> &cr) const {
  cr->save();

  cr->move_to(coordinates.front()[0], coordinates.front()[1]);
  std::for_each(++coordinates.begin(), coordinates.end(),
                [&cr](const std::valarray<double> &coordinatePair) {
                  cr->line_to(coordinatePair[0], coordinatePair[1]);
                });

  cr->set_line_join(Cairo::Context::LineJoin::ROUND);
  cr->set_line_width(30);
  cr->stroke_preserve();
  cr->set_line_width(2.56);
  cr->set_source_rgb(1, 1, 1);
  cr->stroke();

  cr->restore();
}

void DrawableRoad::getRegion(int &x, int &y, int &width, int &height) const {
  x = y = std::numeric_limits<int>::max();
  width = height = 0;
  for (auto &&coordinatePair : coordinates) {
    const auto currentX = static_cast<int>(
        std::floor(coordinatePair[0] - DrawableRoad::width / 2));
    const auto currentY = static_cast<int>(
        std::floor(coordinatePair[1] - DrawableRoad::width / 2));
    x = std::min(currentX, x);
    y = std::min(currentY, y);
    const auto currentWidth = static_cast<int>(
        std::ceil(coordinatePair[0] + DrawableRoad::width / 2 - x));
    const auto currentHeight = static_cast<int>(
        std::ceil(coordinatePair[1] + DrawableRoad::width / 2 - y));
    width = std::max(currentWidth, width);
    height = std::max(currentHeight, height);
  }
}

void DrawableRoad::getCoordinatesOf(std::string_view laneName,
                                    const double positionOnLane, double &x,
                                    double &y) const {
  if (positionOnLane < 0 || positionOnLane > 1) {
    throw std::invalid_argument("positionOnLane is invalid: " +
                                std::to_string(positionOnLane));
  }

  double currentPixelDistance = 0;
  auto currentCoordinatePair =
      laneName == laneThereName ? coordinates.front() : coordinates.back();
  auto nextCoordinatePair = currentCoordinatePair;
  const auto advanceTraversalAndFindNextStep =
      [this, &currentPixelDistance, &currentCoordinatePair,
       positionOnLane](std::valarray<double> coordinatePair) {
        auto nextStep = coordinatePair - currentCoordinatePair;
        auto nextStepLength = std::hypot(nextStep[0], nextStep[1]);
        auto nextStepIsTooFar = currentPixelDistance + nextStepLength >
                                positionOnLane * pixelLength;
        if (!nextStepIsTooFar) {
          currentPixelDistance += nextStepLength;
          currentCoordinatePair = coordinatePair;
        }
        return nextStepIsTooFar;
      };
  if (laneName == laneThereName) {
    nextCoordinatePair = *std::find_if(++coordinates.begin(), coordinates.end(),
                                       advanceTraversalAndFindNextStep);
  } else {
    nextCoordinatePair =
        *std::find_if(++coordinates.rbegin(), coordinates.rend(),
                      advanceTraversalAndFindNextStep);
  }
  const auto nextStep = nextCoordinatePair - currentCoordinatePair;
  const auto nextStepLength = std::hypot(nextStep[0], nextStep[1]);
  auto offset = std::valarray<double>({-nextStep[1], nextStep[0]});
  offset = offset / nextStepLength * 7.5;
  currentCoordinatePair +=
      (positionOnLane * pixelLength - currentPixelDistance) / nextStepLength *
      nextStep;
  currentCoordinatePair += offset;
  x = currentCoordinatePair[0];
  y = currentCoordinatePair[1];
}

bool DrawableRoad::contains(std::string_view laneName) const {
  return laneThereName == laneName || laneBackName == laneName;
}
