#include "DrawableJunction.hpp"
#include <cairomm/enums.h>
#include <cmath>
#include <vector>

const double DrawableJunction::length = 30;

DrawableJunction::DrawableJunction(double xc, double yc)
    : x(xc - length / 2), y(yc - length / 2) {}

void DrawableJunction::draw(const Cairo::RefPtr<Cairo::Context> &cr) const {
  cr->save();

  cr->rectangle(x, y, length, length);
  cr->fill();

  cr->restore();

  cr->save();

  const auto lineWidth = 2;
  cr->set_line_width(lineWidth);
  cr->rectangle(x + lineWidth, y + lineWidth, length - 2 * lineWidth,
                length - 2 * lineWidth);
  cr->set_source_rgb(1, 1, 1);
  cr->set_dash(std::vector<double>({2, 9, 4, 9, 2, 0}), 0);
  cr->set_line_cap(Cairo::Context::LineCap::SQUARE);
  cr->stroke();

  cr->restore();
}

void DrawableJunction::getRegion(int &x, int &y, int &width,
                                 int &height) const {
  x = std::floor(this->x);
  y = std::floor(this->y);
  height = width = std::ceil(length);
}
