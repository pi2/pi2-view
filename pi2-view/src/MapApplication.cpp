#include "MapApplication.hpp"
#include <chrono>
#include <glibmm/i18n.h>
#include <vector>

void MapApplication::initGettext() {
#ifdef G_OS_WIN32
  gchar *win32_dir = g_win32_get_package_installation_directory_of_module(NULL);
  bindtextdomain("pi2-view",
                 g_build_filename(win32_dir, "share", "locale", NULL));
  g_free(win32_dir);
#endif /* G_OS_WIN32 */

  bind_textdomain_codeset("pi2-view", "UTF-8");
}

Glib::RefPtr<Gtk::Application> MapApplication::application = []() {
  initGettext();
  return Gtk::Application::create("de.rwth_aachen.git.pi2.view");
}();
std::thread MapApplication::thread;

void MapApplication::addWindow(Gtk::Window &window) {
  if (application->get_windows().empty()) {
    thread = std::thread([&window]() {
      application = Gtk::Application::create("de.rwth_aachen.git.pi2.view");
      application->signal_activate().connect([&window]() {
        application->add_window(window);
        window.show();
        // TODO: Remove this horrible hack
        std::thread([&window]() {
          window.set_default_size(-1, -1);
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
          window.set_default_size(-1, -1);
          std::this_thread::sleep_for(std::chrono::milliseconds(20));
          window.set_default_size(-1, -1);
          std::this_thread::sleep_for(std::chrono::milliseconds(50));
          window.set_default_size(-1, -1);
          std::this_thread::sleep_for(std::chrono::milliseconds(100));
          window.set_default_size(-1, -1);
          std::this_thread::sleep_for(std::chrono::milliseconds(200));
          window.set_default_size(-1, -1);
        }).detach();
      });
      application->run();
    });
  } else {
    application->add_window(window);
    window.show();
  }
}

void MapApplication::removeWindow(Gtk::Window &window) {
  application->remove_window(window);
  if (thread.joinable() && application->get_windows().empty()) {
    thread.join();
  }
}
