#include "MapArea.hpp"
#include "Drawable.hpp"
#include "DrawableJunction.hpp"
#include "DrawableRoad.hpp"
#include "DrawableVehicle.hpp"
#include <algorithm>
#include <cmath>
#include <gtkmm/widget.h>

const int MapArea::padding = 30;

MapArea::MapArea(std::vector<DrawableJunction> &junctions,
                 std::vector<DrawableRoad> &roads,
                 std::map<std::string, DrawableVehicle> &nameVehicleMap)
    : scale(1), junctions(junctions), roads(roads),
      nameVehicleMap(nameVehicleMap) {
  set_draw_func(sigc::mem_fun(*this, &MapArea::on_draw));
}

void MapArea::adaptSizeRequest() {
  int xJunctions, yJunctions, widthJunctions, heightJunctions;
  Drawable::getRegion(junctions.begin(), junctions.end(), xJunctions,
                      yJunctions, widthJunctions, heightJunctions);

  int xRoads, yRoads, widthRoads, heightRoads;
  Drawable::getRegion(roads.begin(), roads.end(), xRoads, yRoads, widthRoads,
                      heightRoads);

  int width = std::ceil(
      scale *
      (std::max(xJunctions + widthJunctions, xRoads + widthRoads) + padding));
  int height = std::ceil(
      scale *
      (std::max(yJunctions + heightJunctions, yRoads + heightRoads) + padding));
  set_size_request(width, height);
}

void MapArea::setScale(const double scale) {
  this->scale = scale;
  queue_draw();
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void MapArea::on_draw(const Cairo::RefPtr<Cairo::Context> &cr, int width,
                      int height) {
#pragma GCC diagnostic pop
  cr->scale(scale, scale);

  for (auto &&road : roads) {
    road.draw(cr);
  }

  for (auto &&junction : junctions) {
    junction.draw(cr);
  }

  for (auto &&[name, vehicle] : nameVehicleMap) {
    vehicle.draw(cr);
  }
}
