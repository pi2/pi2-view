#pragma once

#include <glibmm/refptr.h>
#include <gtkmm/application.h>
#include <gtkmm/window.h>
#include <thread>

class MapApplication {
public:
  static void addWindow(Gtk::Window &window);
  static void removeWindow(Gtk::Window &window);

private:
  static void initGettext();
  static Glib::RefPtr<Gtk::Application> application;
  static std::thread thread;
};
