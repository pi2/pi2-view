#pragma once

#include "DrawableJunction.hpp"
#include "DrawableRoad.hpp"
#include "DrawableVehicle.hpp"
#include "MapArea.hpp"
#include <chrono>
#include <glibmm/refptr.h>
#include <gtkmm/application.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/label.h>
#include <gtkmm/listbox.h>
#include <gtkmm/listboxrow.h>
#include <gtkmm/window.h>
#include <map>
#include <string>
#include <thread>
#include <valarray>
#include <vector>

class MapWindow : public Gtk::Window {
public:
  MapWindow(std::string windowTitle);
  virtual ~MapWindow();
  void addJunction(double x, double y);
  void addRoad(std::string laneThereName, std::string laneBackName,
               std::vector<std::valarray<double>> coordinates);
  void addOrReplaceBicycle(const std::string &vehicleName,
                           const std::string &laneName, double positionOnLane,
                           double speed);
  void addOrReplaceCar(const std::string &vehicleName,
                       const std::string &laneName, double positionOnLane,
                       double speed, double remainingFuel);
  void setTimeLabel(std::chrono::minutes time);

private:
  static const std::vector<std::string> zoomLevels;
  static Glib::RefPtr<Gtk::Application> app;
  static std::thread thread;
  void addOrReplaceVehicle(const std::string &vehicleName,
                           const std::string &laneName, double positionOnLane,
                           double speed, double remainingFuel, double red,
                           double green, double blue);
  void onZoomSelectionChanged();
  void onVehicleRowSelected(Gtk::ListBoxRow *row);
  void refreshVehicleLabels(const DrawableVehicle &vehicle);
  MapArea mapArea;
  Gtk::ListBox *listBox;
  Gtk::Label *timeLabel;
  Gtk::Label *laneLabel;
  Gtk::Label *lanePositionPercentageLabel;
  Gtk::Label *speedLabel;
  Gtk::Label *remainingFuelLabel;
  Gtk::ComboBoxText *zoomComboBoxText;
  std::vector<DrawableJunction> junctions;
  std::vector<DrawableRoad> roads;
  std::map<std::string, DrawableVehicle> nameVehicleMap;
};
