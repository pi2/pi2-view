#pragma once

#include "Drawable.hpp"
#include <cairomm/context.h>
#include <cairomm/refptr.h>
#include <string>
class DrawableRoad;

class DrawableVehicle : public Drawable {
public:
  DrawableVehicle(std::string carName, std::string laneName,
                  const DrawableRoad &road, double positionOnLane, double speed,
                  double remainingFuel, double red, double green, double blue);

  void draw(const Cairo::RefPtr<Cairo::Context> &cr) const override;
  void getRegion(int &x, int &y, int &width, int &height) const override;

  std::string getLaneName() const;
  double getPositionOnLane() const;
  double getSpeed() const;
  double getRemainingFuel() const;

private:
  const static double radius;

  double xc, yc;
  std::string carName;
  std::string laneName;
  double positionOnLane;
  double speed;
  double remainingFuel;
  double red, green, blue;
};
