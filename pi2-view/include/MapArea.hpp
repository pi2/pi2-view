#pragma once

#include <cairomm/context.h>
#include <cairomm/refptr.h>
#include <gtkmm/drawingarea.h>
#include <map>
#include <string>
#include <vector>
class DrawableJunction;
class DrawableRoad;
class DrawableVehicle;

class MapArea : public Gtk::DrawingArea {
public:
  MapArea(std::vector<DrawableJunction> &junctions,
          std::vector<DrawableRoad> &roads,
          std::map<std::string, DrawableVehicle> &nameVehicleMap);

  void adaptSizeRequest();
  void setScale(double scale);

private:
  void on_draw(const Cairo::RefPtr<Cairo::Context> &cr, int width, int height);

  const static int padding;

  double scale;
  std::vector<DrawableJunction> &junctions;
  std::vector<DrawableRoad> &roads;
  std::map<std::string, DrawableVehicle> &nameVehicleMap;
};
