#pragma once

#include "Drawable.hpp"
#include <cairomm/context.h>
#include <cairomm/refptr.h>

class DrawableJunction : public Drawable {
public:
  DrawableJunction(double xc, double yc);

  void draw(const Cairo::RefPtr<Cairo::Context> &cr) const override;
  void getRegion(int &x, int &y, int &width, int &height) const override;

private:
  const static double length;

  double x, y;
};
