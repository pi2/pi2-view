#pragma once

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * A window showing a map, a list of vehicles and additional information
 */
struct MapWindow;

/**
 * Creates and opens a MapWindow.
 *
 * You *have* to call mapWindowDestroy(MapWindow *) to avoid memory leaks.
 *
 * \returns A pointer to the created MapWindow
 */
MapWindow *mapWindowCreate(const char *windowTitle);

/**
 * Closes and destroys \p mapWindow and frees its memory.
 */
void mapWindowDestroy(MapWindow *mapWindow);

/**
 * Adds a junction to \p mapWindow
 *
 * The coordinates \p x and \p y mark the middle of the junction and represent
 * amounts of pixels.
 */
void mapWindowAddJunction(MapWindow *mapWindow, double x, double y);

/**
 * Adds a road to \p mapWindow
 *
 * \param coordinates A pointer to the begin of an array of the coordinates
 * The coordinates have to be alternating values of x and y in pixels.
 * \param coordinateCount The count of \p coordinates
 */
void mapWindowAddRoad(MapWindow *mapWindow, const char *laneThereName,
                      const char *laneBackName, const double *coordinates,
                      size_t coordinateCount);

/**
 * Adds or replaces a bicycle to/in \p mapWindow
 *
 * \param positionOnLane The relative position of the vehicle on its lane
 * has to be between 0 and 1.
 */
void mapWindowAddOrReplaceBicycle(MapWindow *mapWindow, const char *vehicleName,
                                  const char *roadName, double positionOnLane,
                                  double speed);

/**
 * Adds or replaces a car to/in \p mapWindow
 *
 * \param positionOnLane The relative position of the vehicle on its lane
 * has to be between 0 and 1.
 */
void mapWindowAddOrReplaceCar(MapWindow *mapWindow, const char *vehicleName,
                              const char *roadName, double positionOnLane,
                              double speed, double remainingFuel);

/**
 * Sets the time label of \p mapWindow to \p minutes
 */
void mapWindowSetTimeLabel(MapWindow *mapWindow, int64_t minutes);

#ifdef __cplusplus
}
#endif
