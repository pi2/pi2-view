#pragma once

#include "Drawable.hpp"
#include <cairomm/context.h>
#include <cairomm/refptr.h>
#include <string>
#include <string_view>
#include <valarray>
#include <vector>

class DrawableRoad : public Drawable {
public:
  DrawableRoad(std::string laneThereName, std::string laneBackName,
               std::vector<std::valarray<double>> coordinates);

  void draw(const Cairo::RefPtr<Cairo::Context> &cr) const override;
  void getRegion(int &x, int &y, int &width, int &height) const override;

  void getCoordinatesOf(std::string_view laneName, double positionOnLane,
                        double &x, double &y) const;

  bool contains(std::string_view laneName) const;

private:
  const static double width;

  std::string laneThereName;
  std::string laneBackName;
  double pixelLength;
  std::vector<std::valarray<double>> coordinates;
};
