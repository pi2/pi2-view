#pragma once

#include <algorithm>
#include <cairomm/context.h>
#include <cairomm/refptr.h>
#include <functional>
#include <iterator>
#include <type_traits>

enum class DrawableTrait {
  X,
  Y,
  XPlusWidth,
  YPlusHeight,
};

class Drawable {
public:
  static std::function<bool(const Drawable &drawableA,
                            const Drawable &drawableB)>
  comparator(DrawableTrait drawableTrait);

  template <class ForwardIt>
  static void getRegion(ForwardIt first, ForwardIt last, int &x, int &y,
                        int &width, int &height) {
    using Reference = typename std::iterator_traits<ForwardIt>::reference;
    static_assert(std::is_convertible<Reference, const Drawable &>::value,
                  "We need to work on Drawables");

    int xMinX, yMinX, widthMinX, heightMinX;
    std::min_element(first, last, comparator(DrawableTrait::X))
        ->getRegion(xMinX, yMinX, widthMinX, heightMinX);

    int xMinY, yMinY, widthMinY, heightMinY;
    std::min_element(first, last, comparator(DrawableTrait::Y))
        ->getRegion(xMinY, yMinY, widthMinY, heightMinY);

    int xMaxXPlusWidth, yMaxXPlusWidth, widthMaxXPlusWidth, heightMaxXPlusWidth;
    std::max_element(first, last, comparator(DrawableTrait::XPlusWidth))
        ->getRegion(xMaxXPlusWidth, yMaxXPlusWidth, widthMaxXPlusWidth,
                    heightMaxXPlusWidth);

    int xMaxYPlusHeight, yMaxYPlusHeight, widthMaxYPlusHeight,
        heightMaxYPlusHeight;
    std::max_element(first, last, comparator(DrawableTrait::YPlusHeight))
        ->getRegion(xMaxYPlusHeight, yMaxYPlusHeight, widthMaxYPlusHeight,
                    heightMaxYPlusHeight);

    x = xMinX;
    y = yMinY;
    width = xMaxXPlusWidth + widthMaxXPlusWidth - x;
    height = yMaxYPlusHeight + heightMaxYPlusHeight - y;
  }

  Drawable(const Drawable &other);
  Drawable(Drawable &&other) noexcept;
  Drawable &operator=(const Drawable &other);
  Drawable &operator=(Drawable &&other) noexcept;
  virtual ~Drawable() = 0;

  virtual void draw(const Cairo::RefPtr<Cairo::Context> &cr) const = 0;
  virtual void getRegion(int &x, int &y, int &width, int &height) const = 0;

protected:
  Drawable();
};
