# PI2 View

The GUI library for showing the Map of the Praktikum Informatik 2

## Installation

### Windows

Download the appropriate package from [sciebo](https://tiny.cc/pi2-view_win32) and follow the instructions in the README.

### Arch Linux

PI2 View is available in the [AUR](https://aur.archlinux.org/packages/pi2-view/).

## Dependencies

* [Meson](https://mesonbuild.com/Getting-meson.html) (as build automation tool for this project)

* [gtkmm](https://www.gtkmm.org/en/download.html) (for building the pi2-view GUI in C++)

* [Boost.Program_options](https://www.boost.org/doc/libs/release/libs/program_options/) (for parsing the command line arguments)

* [Git](https://git-scm.com/) (for downloading the [GSL](https://github.com/Microsoft/GSL))

## [Compiling](https://mesonbuild.com/Quick-guide.html#compiling-a-meson-project)
