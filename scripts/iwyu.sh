#!/bin/sh

cd "${MESON_SOURCE_ROOT}"
iwyu_tool -p "${MESON_BUILD_ROOT}" -- --no_comments $@ > "${MESON_BUILD_ROOT}/iwyu-result.txt"
