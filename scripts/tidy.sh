#!/bin/sh

cd "${MESON_SOURCE_ROOT}"
clang-tidy -p "${MESON_BUILD_ROOT}" $@ */src/* > "${MESON_BUILD_ROOT}/clang-tidy-result.txt"
